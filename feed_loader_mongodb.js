const MongoClient = require('mongodb').MongoClient
const BulkOp = require('./lib/bulkop-mongodb')
const tsv = require('./lib/tsv')

var SELLER = 'walmart'
var FILEPATH = '/Users/barenko/Downloads/googleMerchant.txt'
//var MONGODB = 'mongodb://winnie:winnie@ds041651.mongolab.com:41651/product-comparator'
var MONGODB = 'mongodb://localhost:27017/product-comparator'
//var MONGODB = 'mongodb://192.168.99.100:32768/product-comparator'


MongoClient.connect(MONGODB, function(err, db){
	console.time('feedLoader')
	var products = db.collection('products')
	products.ensureIndex({gtin:1}, {unique: true}, function(){})
	var productsBulk = new BulkOp(products)

	var sellers = db.collection('sellers')
	sellers.ensureIndex({seller:1, gtin:1}, {unique: true}, function(){})
	var sellersBulk = new BulkOp(sellers)

	tsv.readGoogleTsv(FILEPATH, SELLER, function(data, dataSeller){
		productsBulk.op({
			gtin:data.gtin
		},
		data, 
		function(err, res){
			if(err) throw err
			console.log(JSON.stringify({inserted:res.nInserted, upserted: res.nUpserted, updated: res.nModified, removed: res.nRemoved}))
		})

		sellersBulk.op({
			seller:dataSeller.seller, gtin:dataSeller.gtin
		}, 
		dataSeller, 
		function(err, res){
			if(err) throw err
			console.log(JSON.stringify({inserted:res.nInserted, upserted: res.nUpserted, updated: res.nModified, removed: res.nRemoved}))
		})

	}, function(){
		productsBulk.execute(function(err, res){
			sellersBulk.execute(function(err, res){
				if(err) throw err
				console.log(JSON.stringify(res))
				db.close(function(err){
					console.timeEnd('feedLoader')
					console.log('finished')
				})
			})
		})
	})
})


