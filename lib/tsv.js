const csv = require("fast-csv")

function readGoogleTsv(filepath, seller, onDataCb, onEndCb){
	csv.fromPath(filepath,{
		trim: true, 
		delimiter: '\t', 
		quote: null, 
		headers: true, 
		strictColumnHandling: true
	})
	.on('data', function(data){
		var gtin = data.gtin||data.ean13||data.isbn
		
		var product = {
			gtin:gtin,
			isbn:data.isbn,
			ean13:data.ean13,
			title:data.title,
			description:data.description,
			image_link:data.image_link,
			link:data.link,
			mobile_link:data.mobile_link,
			category: data.google_product_category,
			shipping_weight: (data.shipping_weight+'').split(' ')[0],
			installment: data.installment,
			importedFrom: seller
		}

		var productSeller = {
			seller:seller,
			gtin:gtin,
			price:data.price,
			availability: data.availability === 'in stock',
			updatedAt: new Date()
		}

		onDataCb(product, productSeller)
	})
	.on('end', onEndCb)
}

module.exports = {
	readGoogleTsv: readGoogleTsv
}