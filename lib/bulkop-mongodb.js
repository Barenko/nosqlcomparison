function BulkOp(collection, bulkSize){
	this.collection = collection
	this.bulk = collection.initializeUnorderedBulkOp()
	this.counter = 0
	this.total = 0
	this.bulkSize = bulkSize || 10000
}

BulkOp.prototype.op = function(query, data, executionCb){
	this.bulk.find(query).upsert().updateOne(data)
	this.counter++

	if(this.counter === this.bulkSize){
		this.execute(executionCb)
		this.bulk = this.collection.initializeUnorderedBulkOp()
	}
}

BulkOp.prototype.execute = function(executionCb){
	if(this.counter > 0){
		this.total += this.counter
		console.log(`Bulk executing for ${this.total} docs ...`)
		this.bulk.execute(executionCb)
		this.counter = 0
	} else if(executionCb) executionCb()
}

module.exports = BulkOp