var SELLER = 'walmart'
var FILEPATH = '/Users/barenko/Downloads/googleMerchant.txt'

const tsv = require('./lib/tsv')
const couchbase = require('couchbase')
const N1qlQuery = require('couchbase').N1qlQuery
const cluster = new couchbase.Cluster('couchbase://127.0.0.1:8091')
const products = cluster.openBucket('products')
const sellers = cluster.openBucket('sellers')

console.time('feedLoader')
tsv.readGoogleTsv(FILEPATH, SELLER, function(data, dataSeller){

	products.upsert(data.gtin, data, function(err, res){
		if(err) throw err
	})
	sellers.upsert(`${dataSeller.seller}_${dataSeller.gtin}`, dataSeller, function(err, res){
		if(err) throw err
	})

}, function(){
	products.disconnect()
	sellers.disconnect()
	console.timeEnd('feedLoader')
	console.log('finished')
})


